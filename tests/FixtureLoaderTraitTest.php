<?php

declare(strict_types=1);

namespace Adrien\Tests;

use Adrien\FixtureLoaderTrait;
use AssertionError;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\EventManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Setup;
use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\TestCase;

class FixtureLoaderTraitTest extends TestCase
{
    public function testItLoadsTheRequestedFixture()
    {
        $objectManager = DummyFactory::createEntityManager();

        $requestedFixture = new class extends DummyFixture {};

        new class($objectManager, $requestedFixture) {
            use FixtureLoaderTrait;
            public function __construct($manager, $fixture)
            {
                $this->loadFixture($manager, $fixture);
            }
        };

        static::assertFixtureLoadCalls($requestedFixture);
    }

    public function testItLoadsSeveralRequestedFixtures()
    {
        $objectManager = DummyFactory::createEntityManager();

        $firstRequestedFixture = new class extends DummyFixture {};
        $secondRequestedFixture = new class extends DummyFixture {};

        new class($objectManager, $firstRequestedFixture, $secondRequestedFixture) {
            use FixtureLoaderTrait;
            public function __construct($manager, $firstRequestedFixture, $secondRequestedFixture)
            {
                $this->loadFixture($manager, $firstRequestedFixture, $secondRequestedFixture);
            }
        };

        static::assertFixtureLoadCalls($firstRequestedFixture);
        static::assertFixtureLoadCalls($secondRequestedFixture);
    }

    public function testItLoadsTheRequestedOrderedFixtures()
    {
        $objectManager = DummyFactory::createEntityManager();

        global $semaphore;
        $semaphore = 0;

        $firstOrderedFixture = new class(1) extends DummyOrderedFixture {};
        $secondOrderedFixture = new class(2) extends DummyOrderedFixture {};

        $semaphore++;

        new class($objectManager, $firstOrderedFixture, $secondOrderedFixture) {
            use FixtureLoaderTrait;
            public function __construct($manager, $firstOrderedFixture, $secondOrderedFixture)
            {
                $this->loadFixture($manager, $secondOrderedFixture, $firstOrderedFixture);
            }
        };

        static::assertSame(3, $semaphore);
        static::assertFixtureLoadCalls($firstOrderedFixture);
        static::assertFixtureLoadCalls($secondOrderedFixture);
    }

    public function testItLoadsTheRequestedDependentFixtures()
    {
        $objectManager = DummyFactory::createEntityManager();

        global $semaphore;
        $semaphore = 0;

        $mainFixture = new class() extends DummyFixture {
            public function load(ObjectManager $manager)
            {
                global $semaphore;
                $semaphore++;
                parent::load($manager);
            }
        };

        $dependentFixture = new class(2, get_class($mainFixture)) extends DummyDependentFixture {};
//        $subDependentFixture = new class(3, get_class($mainFixture)) extends DummyDependentFixture {};
//        $subDependentFixture = new class() extends DummyFixture {};
        $semaphore++;
//        die();

        new class($objectManager, $mainFixture, $dependentFixture) {
            use FixtureLoaderTrait;
            public function __construct($manager, $mainFixture, $dependentFixture)
            {
                $this->loadFixture($manager, $dependentFixture, $mainFixture);
            }
        };

        static::assertSame(3, $semaphore);
        static::assertFixtureLoadCalls($mainFixture);
        static::assertFixtureLoadCalls($dependentFixture);
//        static::assertFixtureLoadCalls($subDependentFixture);
    }

    private static function assertFixtureLoadCalls(DummyFixture $fixture, int $expectedNumberOfCalls = 1): void
    {
        $actualNumberOfCalls = $fixture->calls;
        static::assertSame(
            $expectedNumberOfCalls,
            $actualNumberOfCalls,
            sprintf('The fixture "load" method has been called %d times.', $actualNumberOfCalls)
        );
    }
}
