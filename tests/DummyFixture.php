<?php

declare(strict_types=1);

namespace Adrien\Tests;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DummyFixture implements FixtureInterface
{
    public $calls = 0;

    public function load(ObjectManager $manager)
    {
        $this->calls++;
    }
}
