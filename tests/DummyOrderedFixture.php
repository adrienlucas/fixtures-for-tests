<?php

declare(strict_types=1);

namespace Adrien\Tests;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PHPUnit\Framework\AssertionFailedError;

class DummyOrderedFixture extends DummyFixture implements OrderedFixtureInterface
{
    private $order;

    public function __construct($order = 0)
    {
        $this->order = $order;
    }

    public function load(ObjectManager $manager)
    {
        global $semaphore;
        if ($semaphore !== $this->order) {
            throw new AssertionFailedError('The fixture has not been called at the right time.');
        }
        $semaphore++;
        parent::load($manager);
    }

    public function getOrder()
    {
        return $this->order;
    }
}
